# TestLighthouse

ADDED:
- URL is provided in the .gitlab-ci.yml
- Save output from lighthouse

TODO:
- Change SSL certificate in Dockerfile
- Add Outside Variable
- Publish results to gitlab pages
